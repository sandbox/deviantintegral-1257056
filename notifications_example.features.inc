<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function notifications_example_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function notifications_example_node_info() {
  $items = array(
    'notifications_example' => array(
      'name' => t('Notifications Example'),
      'module' => 'features',
      'description' => t('This content type contains a CCK checkbox field that triggers a custom notification event.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
