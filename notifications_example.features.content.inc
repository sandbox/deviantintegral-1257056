<?php

/**
 * Implementation of hook_content_default_fields().
 */
function notifications_example_content_default_fields() {
  $fields = array();

  // Exported field: field_widget
  $fields['notifications_example-field_widget'] = array(
    'field_name' => 'field_widget',
    'type_name' => 'notifications_example',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'off|Widget deactivated
on|Widget activated',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => 'off',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Widget',
      'weight' => '-4',
      'description' => 'Flip this CCK field on and off to trigger notifications on this node.',
      'type' => 'optionwidgets_onoff',
      'module' => 'optionwidgets',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Widget');

  return $fields;
}
